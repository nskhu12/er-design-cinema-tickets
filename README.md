# ER Design

The purpose of this task is to sketch out the design of a database. You will practice creating an ER diagram by defining the entities, their attributes and showing their relationships.

## system to design an ER diagram: Cinema tickets

Give a high-level overview of what functions and use cases are relevant for the chosen system.
Design entities and their attributes, relationships between them. Describe them.
Draw an ER diagram depicting the designed entities, attributes (with data types), and relationships.
